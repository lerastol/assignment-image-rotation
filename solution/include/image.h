#pragma once
#include <stdint.h>

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
  uint64_t width, height;
  struct pixel* data;
};

int init_image(struct image* image, uint64_t width, uint64_t height);

void destroy_image(struct image* image);
