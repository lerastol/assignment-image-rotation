#pragma once

struct image;

enum load_status {
  LOAD_OK = 0,
  FILE_OPEN_ERROR = -1
};

enum save_status {
	SAVE_OK = 0,
	FILE_SAVE_ERROR = -1
};

enum load_status load_image(char* const filename, struct image* image);

enum save_status save_image(char* const filename, struct image const* image);
