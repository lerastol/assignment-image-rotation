#include "../include/bmp.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>


#define READ_HEADER_COUNT 1
#define BF_TYPE_CORRECT 0x4D42
#define BF_RESERVED_CORRECT 0
#define RGB_BIT_COUNT 24

static inline uint64_t get_padding(uint64_t width_bytes) {
    return width_bytes % 4 == 0 ? 0 : 4 - width_bytes % 4;
}

enum read_status from_bmp(FILE* in, struct image* image) {
    uint64_t header_size = sizeof(struct bmp_header);
    uint64_t pixel_size = sizeof(struct pixel);
    struct bmp_header header;
    uint64_t readed = fread(&header, header_size, READ_HEADER_COUNT, in);
    if (readed != READ_HEADER_COUNT)
        return READ_INVALID_HEADER;
    if (header.bfType != BF_TYPE_CORRECT)
        return READ_INVALID_SIGNATURE;
    if (header.bfReserved != BF_RESERVED_CORRECT)
        return READ_INVALID_SIGNATURE;
    if (header.biBitCount != RGB_BIT_COUNT)
        return UNSUPPORTED_BIT_RATE;
    if (!init_image(image, header.biWidth, header.biHeight))
        return MEMORY_ALLOCATION_ERROR;
    uint64_t image_size = image->width * image->height;
    uint64_t width_bytes = image->width * pixel_size;
    uint64_t padding = get_padding(width_bytes);
    readed = 0;
    fseek(in, (long)(header.bOffBits - header_size), SEEK_CUR);
    for (uint64_t i = 0; i < image->height && !feof(in); i++)
    {
        readed += fread(image->data + i * image->width,
            pixel_size, image->width, in);
        fseek(in, (long)padding, SEEK_CUR);
    }
    if (readed != image_size)
        return READ_INVALID_SIGNATURE;
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* image) {
    uint32_t header_size = sizeof(struct bmp_header);
    uint64_t pixel_size = sizeof(struct pixel);
    uint32_t image_size = (uint32_t)(image->width * image->height);
    uint32_t image_size_bytes = (uint32_t)(image_size * pixel_size);
    struct bmp_header header = {0};
    header.bfType = BF_TYPE_CORRECT;
    header.biWidth = (uint32_t)image->width;
    header.biHeight = (uint32_t)image->height;
    header.biBitCount = 24;
    header.biPlanes = 1;
    header.bOffBits = header_size;
    header.biSizeImage = image_size_bytes;
    header.bfileSize = header.bOffBits + header.biSizeImage;
    header.biXPelsPerMeter = 0xEC4;
    header.biYPelsPerMeter = 0xEC4;
    header.biSize = 40;
    if (fwrite(&header, header_size, 1, out) != 1)
        return WRITE_ERROR;
    uint64_t width_bytes = image->width * pixel_size;
    uint64_t padding = get_padding(width_bytes);
    for (uint64_t i = 0; i < image->height; i++)
    {
        if (fwrite(image->data + i * image->width, pixel_size,
            image->width, out) != image->width)
            return WRITE_ERROR;
        for (uint64_t j = 0; j < padding; j++)
            if(fputc(0, out) != 0)
                return WRITE_ERROR;
    }
    return WRITE_OK;
}
