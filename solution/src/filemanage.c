#include "../include/filemanage.h"
#include "../include/bmp.h"
#include "../include/image.h"
#include <stdio.h>

enum load_status load_image(char* const filename, struct image* image) {
    FILE* file = fopen(filename, "rb");
    enum load_status status;
    if (file != NULL)
    {
        enum read_status read_status = from_bmp(file, image);
        if (read_status == READ_OK)
            status = LOAD_OK;
        else 
            status = (enum load_status)read_status;
        fclose(file);
    }
    else
        status = FILE_OPEN_ERROR; 
    return status;
}

enum save_status save_image(char* const filename, struct image const* image)
{
    FILE* file = fopen(filename, "wb");
    enum save_status status;
    if (file != NULL)
    {
        enum write_status write_status = to_bmp(file, image);
        if (write_status == WRITE_OK)
            status = SAVE_OK;
        else 
            status = (enum save_status)write_status;
        fclose(file);
    }
    else
        status = FILE_SAVE_ERROR;
    return status;
}
