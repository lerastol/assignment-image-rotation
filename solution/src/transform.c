#include "../include/transform.h"
#include "../include/bmp.h"
#include "../include/image.h"

struct image rotate(struct image const source) {
    uint64_t width = source.width;
    uint64_t height = source.height;
    struct image image = { 0 };
    if (init_image(&image, height, width)) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                image.data[x * height + (height - y - 1)] =
                    source.data[y * width + x];
            }
        }
    }
    return image;
}
