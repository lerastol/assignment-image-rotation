#include "../include/image.h"
#include <stdlib.h>

int init_image(struct image* image,
    uint64_t width, uint64_t height) {
    uint64_t pixel_size = sizeof(struct pixel);
    uint64_t size = pixel_size * width * height;
    struct pixel* data = (struct pixel*)malloc(size);
    if (!data)
        return 0;
    image->width = width;
    image->height = height;
    image->data = data;
    return 1;
}

void destroy_image(struct image* image) {
    if (image->data != NULL)
    {
        free((void*)image->data);
        image->data = NULL;
    }
}
