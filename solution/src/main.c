#include "../include/filemanage.h"
#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/transform.h"
#include <stdio.h>

#define PROGRAMM_CORRECT_ARGC 3
#define INPUT_IMAGE_INDEX 1
#define OUTPUT_IMAGE_INDEX 2

int main(const int argc, char** argv)
{
    (void)argc; (void)argv;
    if (argc != PROGRAMM_CORRECT_ARGC)
    {
        printf("%s", "Use: image-transformer <input_path> <output_path>");
        return 0;
    }
    char* inputPath = argv[INPUT_IMAGE_INDEX];
    char* outputPath = argv[OUTPUT_IMAGE_INDEX];
    struct image image = {0};
    enum load_status load_status = load_image(inputPath, &image);
    if (load_status != LOAD_OK)
    {
        switch (load_status)
        {
        case FILE_OPEN_ERROR:
            fprintf(stderr, "The file '%s' could not be opened.", inputPath);
            return 1;
        default:
            switch ((enum read_status)load_status)
            {
            case READ_INVALID_SIGNATURE:
                fprintf(stderr, "The file '%s' has an invalid signature.", inputPath);
                destroy_image(&image);
                return 2;
            case READ_INVALID_BITS:
                fprintf(stderr, "The file '%s' has incorrect bit values.", inputPath);
                destroy_image(&image);
                return 3;
            case READ_INVALID_HEADER:
                fprintf(stderr, "The file '%s' has an incorrect header", inputPath);
                destroy_image(&image);
                return 4;
            case MEMORY_ALLOCATION_ERROR:
                fprintf(stderr, "Memory allocation error.");
                destroy_image(&image);
                return 5;
            case UNSUPPORTED_BIT_RATE:
                fprintf(stderr, "Unsupported bit rate error.");
                destroy_image(&image);
                return 5;
            default:
                break;
            }
            break;
        }
    }
    struct image rotatedImage = rotate(image);
    enum save_status save_status = save_image(outputPath, &rotatedImage);
    if (save_status != SAVE_OK) {
        if (save_status == FILE_SAVE_ERROR) {
            fprintf(stderr, "The file '%s' could not be opened to write.", outputPath);
            destroy_image(&image);
            destroy_image(&rotatedImage);
            return 6;
        } else if ((enum write_status)save_status == WRITE_ERROR) {
            fprintf(stderr, "Error writing to the file '%s'.", outputPath);
            destroy_image(&image);
            destroy_image(&rotatedImage);
            return 7;
        }
    }
    destroy_image(&image);
    destroy_image(&rotatedImage);
    return 0;
}
